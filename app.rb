require 'sinatra'
require 'redis'
require 'json'
require 'erb'

class Application < Sinatra::Base
	@@redis = Redis.new(host: "main.gv12hq.0001.use1.cache.amazonaws.com", port: 6379)

	helpers do
		def inventory_list
			keys = @@redis.keys("inv:*")

			keys.map { |key|
				/inv:(.+)/.match(key).captures[0]
			}
		end

		def inventory_item (item)
			@@redis.get("inv:#{item}")
		end

		def wrap_json (code, response = "")
			{
				meta: {
					code: code,
					status: ""
				},
				response: response
			}.to_json
		end
	end

	get '/inventory/list' do
		wrap_json(200, inventory_list)		
	end

	get '/inventory/list/verbose' do
		wrap_json(200, inventory_list.map { |key|
			{
				key: key,
				quantity: inventory_item(key)
			}
		})
	end

	get '/inventory/item/:item' do
		wrap_json(200, inventory_item(params['item']))
	end

	post '/inventory/add/:item/:quantity' do
		@@redis.incrby("inv:#{params['item']}", params['quantity'])

		wrap_json(200)
	end

	post '/inventory/remove/:item/:quantity' do
		# this resource will need to be locked to prevent race conditions in future

		in_stock = @@redis.get("inv:#{params['item']}")
		requested = params['quantity']	

		if in_stock < requested
			return wrap_json(403)
		end

		@@redis.decrby("inv:#{params['item']}", requested)

		wrap_json(200)
	end
end

if __FILE__ == $0
	run Application.run!
end
